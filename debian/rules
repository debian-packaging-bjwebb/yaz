#!/usr/bin/make -f
# -*- makefile -*-

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

DH=dh --with quilt --with autotools_dev

#export PATH:=${HOME}/ccache:${PATH}

export CFLAGS+= -Wall

%:
	$(DH) $@

override_dh_installchangelogs:
	dh_installchangelogs NEWS

override_dh_auto_configure:
	autoreconf -vif
	dh_auto_configure -- \
		--enable-shared --enable-static \
		--enable-tcpd --with-xslt \
		--with-gnutls --without-openssl \
		--with-icu --with-xml2 --with-pcap

override_dh_auto_clean:
	dh_auto_clean
	# remove stamp files
	$(RM) stamp-*

clean:
	$(DH) clean
	# remove generated files
	$(RM) doc/*.html doc/*.7 doc/*.1 doc/*.8 \
		doc/htmlhelp.hhp doc/toc.hhc doc/manref.xml
	# remove autoconf generated files
	find . -name Makefile.in -exec $(RM) {} \;
	$(RM) aclocal.m4 configure config/* m4/libtool.m4 m4/lt*.m4
	$(RM) yaz.spec doc/local.ent doc/common/print.dsl \
		yaz-config yaz.pc Doxyfile win/version.nsi \
		include/yaz/yaz-version.h
	# if this is a git repository, restore removed files that would have
	# been ignored by dpkg-source
	-test -d .git && git checkout -- $$(git status | \
		sed -e '/^#[[:space:]]*deleted:[[:space:]]*/s/^#[[:space:]]*deleted:[[:space:]]*//p;d' | \
		grep -v '^debian/')

override_dh_auto_install:
	dh_auto_install
	mv debian/tmp/usr/share/doc/yaz debian/tmp/usr/share/doc/yaz-doc

override_dh_installdocs:
	dh_installdocs -A README

override_dh_shlibdeps:
	dh_shlibdeps -a -- --warnings=7

.PHONY: clean

